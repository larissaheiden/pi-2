package br.ifsc.edu.br.categoria;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

public class categoria1 extends AppCompatActivity {

    ArrayList<String> item;
    ArrayAdapter<String> adapter;

    EditText addItem;
    Button btnAdd;
    ListView ListView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categoria1);


            ListView = findViewById(R.id.ListView);
            addItem = findViewById(R.id.addItem);
            btnAdd = findViewById(R.id.btnAdd);

            item = new ArrayList<>();
            adapter = new ArrayAdapter<String>(categoria1.this, android.R.layout.simple_list_item_multiple_choice, item);

            View.OnClickListener addList = new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    item.add(addItem.getText().toString());
                    addItem.setText("");

                    adapter.notifyDataSetChanged();

                }
            };

            btnAdd.setOnClickListener(addList);

            ListView.setAdapter(adapter);

    }


}
